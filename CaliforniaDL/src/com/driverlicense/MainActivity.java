package com.driverlicense;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends android.support.v4.app.FragmentActivity {

	private static final int ITEM_ROUTE_A = 0;
	private static final int ITEM_ROUTE_B = 1;
	private static final int ITEM_ROUTE_C = 2;
	
	
	
	private static final int ROUTE_COUNT = 3;
	
	public class LocationPoint implements Serializable {
		 
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private double latitude;
		 
		 private double longitude;
		 
		 public LocationPoint() {
		 
		 }
		 
		 public LocationPoint(Double latitude, Double longitude) {
		  this.latitude = latitude;
		  this.longitude = longitude;
		 }
		  
		 /**
		  * @return the latitude
		  */
		 public double getLatitude() {
		  return latitude;
		 }
		 
		 /**
		  * @return the longitude
		  */
		 public double getLongitude() {
		  return longitude;
		 }
	}

	static class UiHandler extends Handler {
	    private WeakReference<MainActivity> mService =null; 

	    UiHandler(MainActivity service) {
	        mService = new WeakReference<MainActivity> (service);
	    }
	    @Override
	    public void handleMessage(Message msg)
	    {
	    	MainActivity service = mService.get();
	         if (service != null) {
	              service.handleMessage(msg);
	         }
	    }
	}
	
	public class stepinfo  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public ArrayList<LocationPoint> poly;
		public String instructions;
	}
	public class PathDatas  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public String splimit;
		public String sDist;
		public ArrayList<stepinfo> steps;
	}
	
   // private MapView mMapView;
    private GoogleMap mMap;
    private UiSettings mUiSettings;
    private static final LatLng CA_DVM = new LatLng(37.34995829, -121.99330933);
   
    private ArrayList<PolylineOptions> optionslist = new ArrayList<PolylineOptions>(); 
    private int[] clrArray = {Color.BLUE, Color.CYAN, Color.GREEN, Color.RED, Color.MAGENTA, Color.GRAY};
    
    private int mnCurPath = 0;
    
    private Criteria criteria;
    private LocationManager locationManager;
    private android.location.Location location;
    
    private TextView tvPosition;
    private TextView tvSpeed;
    private TextView tvBearing;
    private static UiHandler mHandler;
    //private ArrayList<PathDatas> pathdata = new ArrayList<PathDatas>();
    private ArrayList<ArrayList<PathDatas>> routes = new ArrayList<ArrayList<PathDatas>>();
    
    private CalcThread mThread;
    
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
		
		setUpMapIfNeeded();
        
//        mTextView = (TextView)findViewById(R.id.tvShow);
//        mTextView.setText(R.string.hello_world);
        
        tvPosition = (TextView)findViewById(R.id.tvShow);
        tvSpeed = (TextView)findViewById(R.id.tvShowInfo);
        tvBearing = (TextView)findViewById(R.id.tvShowturn);

        mHandler = new UiHandler(this);
        
        getLocation();
        
	}
	
	public void handleMessage(Message msg) {
	    switch (msg.what) {
	        case UPDATE_SPEED:
	        	tvSpeed.setText((String) msg.obj);
	            break;
	        case UPDATE_LATLNG:
	        	tvPosition.setText(Html.fromHtml((String) msg.obj));
	            break;
	        case UPDATE_BEARING:
	        	tvBearing.setText((String) msg.obj);
	        	break;
	    }
	}

	private final LocationListener locationListener = new LocationListener()  
    {  
               //当位置改变时调用下面的函数   
		@Override
		public void onLocationChanged(android.location.Location arg0) {
			updateToNewLocation(arg0);     
		}  
        //当Provider不可用时调用下面的函数     
        @Override  
        public void onProviderDisabled(String arg0)  
        {  
              
        }  
                  
        @Override  
        public void onProviderEnabled(String arg0)  
        {  
          
        }  
        @Override  
        public void onStatusChanged(String arg0, int arg1, Bundle arg2)  
        {  
            //updateToNewLocation(null);     
        }
    };  
    
    public static final int UPDATE_SPEED = 1;
    public static final int UPDATE_LATLNG = 2;
    public static final int UPDATE_BEARING = 3;

    private static final int TEN_SECONDS = 10000;
    private static final int TEN_METERS = 10;
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    
	private final GpsStatus.Listener statusListener = new GpsStatus.Listener() {

		@Override
		public void onGpsStatusChanged(int event) {
			// 获取GPS卫星信息，与获取location位置信息一样，还是通过locationManager类，方法为getGpsStatus，返回的是一个GpsStatus类型的结构
			GpsStatus gpsStatus = locationManager.getGpsStatus(null);
			// 触发事件event
			switch (event) {
			case GpsStatus.GPS_EVENT_STARTED:

				break;
			// 第一次定位时间
			case GpsStatus.GPS_EVENT_FIRST_FIX:

				break;
			// 收到卫星信息，并调用DrawMap()函数，进行卫星信号解析并显示到屏幕上
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:

				break;

			case GpsStatus.GPS_EVENT_STOPPED:
				break;
			}
		}
	};

    private void updateToNewLocation(Location location)   
    {         
        if (location != null)  
        {     
        	float  bearing = (float)location.getBearing();
            float  latitude = (float)location.getLatitude();      //维度   
            float longitude= (float)location.getLongitude();     //经度    
            float GpsSpeed = (float)location.getSpeed();    //速度       
            float GpsAlt = (float)location.getAltitude();       //海拔   
            
            Message.obtain(mHandler,
                    UPDATE_LATLNG,
                    latitude + ", " + longitude).sendToTarget();
            Message.obtain(mHandler,
            		UPDATE_SPEED,
            		GpsSpeed+"m/s").sendToTarget();
            Message.obtain(mHandler,
            		UPDATE_BEARING,
            		bearing+"*").sendToTarget();
            
            
            mThread = new CalcThread(routes.get(mnCurPath), mHandler, location);
            mThread.start();

        }  
        else{
            Message.obtain(mHandler,
                    UPDATE_LATLNG,
                    getString(R.string.unkown)).sendToTarget();
            Message.obtain(mHandler,
            		UPDATE_SPEED,
            		getString(R.string.unkown)).sendToTarget();
            Message.obtain(mHandler,
            		UPDATE_BEARING,
            		getString(R.string.unkown)).sendToTarget();
        	
        }
    }  
    
	private void getLocation()      
    {      
        // 查找到服务信息    位置数据标准类  
        criteria = new Criteria();      
        //查询精度:高  
        criteria.setAccuracy(Criteria.ACCURACY_FINE);   
        // 是否查询海拔:是  
        criteria.setAltitudeRequired(true);  
        //是否查询方位角:是  
        criteria.setBearingRequired(true);  
        //是否允许付费  
        criteria.setCostAllowed(true);      
        // 电量要求:低  
        criteria.setPowerRequirement(Criteria.POWER_LOW);  
        //是否查询速度:是  
        criteria.setSpeedRequired(true);  
          
        String provider = locationManager.getBestProvider(criteria, true);  
          
        // 获取GPS信息   获取位置提供者provider中的位置信息  
        location = locationManager.getLastKnownLocation(provider);   
        // 通过GPS获取位置       
        updateToNewLocation(location);     
        // 设置监听器，自动更新的最小时间为间隔N秒(1秒为1*1000，这样写主要为了方便)或最小位移变化超过N米      
        //实时获取位置提供者provider中的数据，一旦发生位置变化 立即通知应用程序locationListener  
        locationManager.requestLocationUpdates(provider,  TEN_SECONDS, TEN_METERS,  locationListener);  
        //监听卫星，statusListener为响应函数  
        locationManager.addGpsStatusListener(statusListener);  
    }  
	
	public ArrayList<LocationPoint> decodePoly(String encoded) {
    		  ArrayList<LocationPoint> poly = new ArrayList<LocationPoint>();
		  int index = 0, len = encoded.length();
		  int lat = 0, lng = 0;
		  while (index < len) {
		   int b, shift = 0, result = 0;
		   do {
		    b = encoded.charAt(index++) - 63;
		    result |= (b & 0x1f) << shift;
		    shift += 5;
		   } while (b >= 0x20);
		   int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
		   lat += dlat;
		   shift = 0;
		   result = 0;
		   do {
		    b = encoded.charAt(index++) - 63;
		    result |= (b & 0x1f) << shift;
		    shift += 5;
		   } while (b >= 0x20);
		   int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
		   lng += dlng;
		   LocationPoint p = new LocationPoint((((double) lat / 1E5)),(((double) lng / 1E5)));
		   poly.add(p);
		  }
		  return poly;
	}

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            //mMap = ((MapView) findViewById(R.id.map)).getMap();
        	Fragment fm = getSupportFragmentManager().findFragmentById(R.id.map);
        	if (fm != null)
        	{
	        	mMap = ((SupportMapFragment)fm).getMap();
	            if (mMap != null) {
	                setUpMap();
	            }
        	}
        }
        
//        LatLng SYDNEY = new LatLng(-33.87365, 151.20689);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));
    }

    private void getPoints(){
    	InputStream inputStream = getResources().openRawResource(R.raw.datapath);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.v("Text Data", byteArrayOutputStream.toString());
        try {

            // Parse the data into jsonobject to get original data in form of json.  
        	int clrIndex = 0;
            JSONArray jArray = new JSONArray(byteArrayOutputStream.toString());   
            for (int i = 0; i < jArray.length(); i++) {
            	JSONArray jArr = jArray.getJSONArray(i);
            	PolylineOptions option = new PolylineOptions();
            	if(clrIndex < clrArray.length)
            	{
            		option.color(clrArray[clrIndex]);
            		clrIndex ++;
            	}
            	else
            	{
            		option.color(clrArray[0]);
            		clrIndex = 1;
            	}
            	optionslist.add(option);
            	for(int j = 0; j < jArr.length(); j++){
            		ArrayList<LocationPoint> poly = decodePoly(jArr.getString(j));
            		 for (int ii = 0; ii < poly.size(); ii++) {
            	            option.add(new LatLng(poly.get(ii).latitude, poly.get(ii).longitude));
            	        }
            	}
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void getPointsfile(){
    	InputStream inputStream = getResources().openRawResource(R.raw.allpath);
    	
    	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    	
    	int ctr;
    	try {
    		ctr = inputStream.read();
    		while (ctr != -1) {
    			byteArrayOutputStream.write(ctr);
    			ctr = inputStream.read();
    		}
    		inputStream.close();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	Log.v("Text Data", byteArrayOutputStream.toString());
    	try {
    		
    		// Parse the data into jsonobject to get original data in form of json.  
    		int clrIndex = 0;
    		JSONArray jArray = new JSONArray(byteArrayOutputStream.toString());   
    		for (int i = 0; i < jArray.length(); i++) {
    			JSONArray jArr = jArray.getJSONArray(i);
    			
				PolylineOptions option = new PolylineOptions();
				if(clrIndex < clrArray.length)
				{
					option.color(clrArray[clrIndex]);
					clrIndex ++;
				}
				else
				{
					option.color(clrArray[0]);
					clrIndex = 1;
				}
				optionslist.add(option);    	
				ArrayList<PathDatas> pathdata = new ArrayList<PathDatas>();
				routes.add(pathdata);
    			
    			for(int j = 0; j < jArr.length(); j++){
    				JSONObject jb = (JSONObject) jArr.get(j);
    				PathDatas pd = new PathDatas();
    				pd.splimit = jb.getString("speedlimit");
    				pd.sDist = jb.getString("distance");
    				pd.steps = new ArrayList<stepinfo>();
    				pathdata.add(pd);

    				JSONArray steps = jb.getJSONArray("steps");
    				for(int k = 0; k < steps.length(); k++){
    					JSONObject jbb = (JSONObject) steps.get(k);
    					stepinfo si = new stepinfo();
    					si.poly = decodePoly(jbb.getString("points"));
    					si.instructions = jbb.getString("instructions");
    					pd.steps.add(si);
    					
	    				for (int ii = 0; ii < si.poly.size(); ii++) {
	    					option.add(new LatLng(si.poly.get(ii).latitude, si.poly.get(ii).longitude));
	    				}
    				}
    			}
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private void setUpMap() {
	   	mUiSettings = mMap.getUiSettings();
    	mUiSettings.setMyLocationButtonEnabled(true);
    	//mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    	mMap.setMyLocationEnabled(true);

        //getPoints();
    	getPointsfile();
//        mMap.addPolyline(optionslist.get(0));
//        mMap.addPolyline(optionslist.get(1));
//        mMap.addPolyline(optionslist.get(2));
       
        selectCurPath(mnCurPath);
    }

    private void selectCurPath(int nPath){
    	if(nPath < ROUTE_COUNT){
    		mnCurPath = nPath;
    		mMap.clear();
    		
        	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CA_DVM, 15));
        	mMap.addMarker(new MarkerOptions().position(CA_DVM).title("DMV"));

        	mMap.addPolyline(optionslist.get(mnCurPath));
    	}
    	
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
	public boolean onMenuOpened(int featureId, Menu menu) {
    	FragmentManager fm = getSupportFragmentManager();
    	final MyMenu dlg = MyMenu.newInstance();
    	dlg.setOnClickListener(new MyMenu.DialogInterface() {
			
			@Override
			public void MenuClick(View arg1, int arg2, long arg3) {
				//Toast.makeText(getApplicationContext(), "adb = "+arg2, Toast.LENGTH_LONG).show();
				switch(arg2){
				case ITEM_ROUTE_A:
				case ITEM_ROUTE_B:
				case ITEM_ROUTE_C:
					selectCurPath(arg2);
					break;
				}
				
				dlg.dismiss();
			}
		});
        dlg.show(fm, "fragment_menu");
		return false;
	}

	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.activity_main, menu);
		menu.add("menu");// 必须创建一项
		return true;
	}

}
