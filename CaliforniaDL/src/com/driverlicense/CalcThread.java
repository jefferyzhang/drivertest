package com.driverlicense;

import java.util.ArrayList;

import android.location.Location;
import android.os.Handler;
import android.os.Message;

import com.driverlicense.MainActivity.PathDatas;
import com.driverlicense.MainActivity.stepinfo;

public class CalcThread extends Thread {
	
	static Object objLock = new Object();
	ArrayList<PathDatas> mpathdata;
	Handler hander;
	Location mcurloc;
	boolean bFirst ;
	
	public float mCurDistance = 1000000;
	public Location mCurpathLoc;
	public String sTurnS="";
	
	CalcThread(ArrayList<PathDatas> pathdata, Handler h, Location loc){
		mpathdata = pathdata;
		hander = h;
		mcurloc = loc;
		bFirst = true;
	}

	@Override
	public void run() {
		synchronized(objLock){
			Location loc = new Location("");
			float ldist = 0;
			for(PathDatas pd: mpathdata){
				for(stepinfo si: pd.steps){
					if(si.poly.size() > 0){
						if(bFirst){
							loc.setLatitude(si.poly.get(0).getLatitude());
							loc.setLongitude(si.poly.get(0).getLongitude());
							mCurDistance = mcurloc.distanceTo(loc);
							mCurpathLoc = new Location(loc);
							sTurnS = si.instructions;
							bFirst = false;
						}else{
							loc.setLatitude(si.poly.get(0).getLatitude());
							loc.setLongitude(si.poly.get(0).getLongitude());
							//mCurDistance = Math.min(mCurDistance, mcurloc.distanceTo(loc));
							ldist = mcurloc.distanceTo(loc);
							if(mCurDistance > ldist){
								mCurpathLoc.set(loc);
								mCurDistance = ldist;
								sTurnS = si.instructions;
							}

							loc.setLatitude(si.poly.get(si.poly.size()-1).getLatitude());
							loc.setLongitude(si.poly.get(si.poly.size()-1).getLongitude());
							ldist = mcurloc.distanceTo(loc);
							if(mCurDistance > ldist){
								mCurpathLoc.set(loc);
								mCurDistance = ldist;
								sTurnS = si.instructions;
							}

						}
					}
				}
			}
			 Message.obtain(hander,
					 MainActivity.UPDATE_LATLNG,
	                    sTurnS).sendToTarget();
		}
		
		super.run();
	}
	
	
}
