package com.driverlicense;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.SimpleAdapter;

public class MyMenu extends DialogFragment {
    
    AlertDialog menuDialog;// menu菜单Dialog
    GridView menuGrid;
    
    public interface DialogInterface{
    	void MenuClick(View arg1, int arg2, long arg3);
    }
    
    
    
    DialogInterface dlginterface;
    
    static MyMenu newInstance() {
        return new MyMenu();
    }
    
    void setOnClickListener(DialogInterface dm){
    	dlginterface = dm;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setTitle(R.string.menutitle);
		Window window = getDialog().getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);  
		WindowManager.LayoutParams wmlp =window.getAttributes();
		wmlp.gravity = Gravity.BOTTOM;
		window.setAttributes(wmlp);
	
		
		View view = inflater.inflate(R.layout.gridview_menu, container, false);
		Resources res =getResources();
		String[] menu_name_array=res.getStringArray(R.array.mainmenu);
		TypedArray ar = res.obtainTypedArray(R.array.menu_image);
		int len = ar.length();
		int[] menu_image_array = new int[len];
		for (int i = 0; i < len; i++)
		{
    		menu_image_array[i] = ar.getResourceId(i, 0);
    	}
		ar.recycle();
		
		
		
		menuGrid = (GridView) view.findViewById(R.id.gridview);
	    menuGrid.setAdapter(getMenuAdapter(view, menu_name_array, menu_image_array));
	    
	    menuGrid.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
            	Log.d("Test", "arg2="+arg2+";arg3="+arg3);
            	dlginterface.MenuClick(arg1, arg2, arg3);
                
            }
        });
	    
		return view;
	}

	 private SimpleAdapter getMenuAdapter(View v, String[] menuNameArray,
	            int[] imageResourceArray) {
	        ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
	        for (int i = 0; i < menuNameArray.length; i++) {
	            HashMap<String, Object> map = new HashMap<String, Object>();
	            map.put("itemImage", imageResourceArray[i]);
	            map.put("itemText", menuNameArray[i]);
	            data.add(map);
	        }
	        SimpleAdapter simperAdapter = new SimpleAdapter(v.getContext(), data,
	                R.layout.item_menu, new String[] { "itemImage", "itemText" },
	                new int[] { R.id.item_image, R.id.item_text });
	        return simperAdapter;
	    }
	 
	 
}
