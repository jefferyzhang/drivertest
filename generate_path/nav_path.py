#kml->coordinates->navigate
import json, urllib2

#make points path on Google map
#download as KML file
#copy coordinates to s


def get_coordinates(kmlstr):    
    (longitude, latitude, altitude) = kmlstr.strip().split(",")
    return "%s,%s"%(latitude, longitude)
    




def query(orgin, destination, wps, drawlines):
    #"&language=zh-cn"    
    url="http://maps.googleapis.com/maps/api/directions/json?sensor=true&origin=%s&destination=%s&waypoints=%s"% \
        (orgin, destination, "|".join(wps))
    #print "Query Google direction API:", url

    resp=urllib2.urlopen(url)
    navdata = json.loads(resp.read())

    if navdata.get("status") != "OK":
        raise Exception("nav web api error")
    routes = navdata.get("routes")
    if len(routes) != 1:
        raise Exception("routes number exception")
    myroute = routes[0] #MUST BE 1 route, because we did not set alternatives in request

    
    

    legs = myroute.get("legs")
    for leg in legs:
        print "distance:", leg.get("distance").get("text")
        print "duration", leg.get("duration").get("text")
        print "from:", leg.get("start_address"), "\nto:", leg.get("end_address")
        for step in leg.get("steps"):
            print "*", step.get("html_instructions")
            drawlines.append(step.get("polyline").get("points"))
        print "-" * 8, "one leg over\n"

    
        
turn = {'south-east': 'L', 'east-north': 'L',
        'south-west': 'R', 'west-south': 'L',
        'north-west': 'L', 'west-north': 'R',
        'east-south': 'R', 'north-east': 'R'}

def run(s):
    coordinates = map(get_coordinates, s.strip().split("\n"))
    #orgin= coordinates[0]
    #destination = coordinates[-1]
    #waypoints =  coordinates[1:-1]

    print len(coordinates)
    print "\n\n== NAV START ==\n\n"
    WP_LIMIT= 8 #1~8
    drawlines = []
    for i in xrange(0, len(coordinates)/WP_LIMIT+1):    
        pts = coordinates[i*WP_LIMIT: (i+1)*WP_LIMIT]
        if len(pts) != 0:        
            orgin = pts[0] #first point
            destination = pts[-1] #last point
            waypoints = pts[1:-1] #mid points
            query(orgin, destination, waypoints, drawlines)
    import json
    print json.dumps(drawlines)





around_company="""
-122.038094,37.387573,0.000000
        -122.038254,37.387138,0.000000
        -122.035477,37.386551,0.000000
        -122.034065,37.390831,0.000000
        -122.036758,37.391727,0.000000
        -122.038094,37.387573,0.000000
"""
dmv_test_2='''
-121.994011,37.349098,0.000000
        -121.991760,37.349152,0.000000
        -121.989906,37.349819,0.000000
        -121.987633,37.350422,0.000000
        -121.986778,37.350479,0.000000
        -121.986778,37.350479,0.000000
        -121.986778,37.352280,0.000000
        -121.986778,37.352280,0.000000
        -121.977928,37.352291,0.000000
        -121.977928,37.352291,0.000000
        -121.977661,37.351971,0.000000
        -121.977669,37.346958,0.000000
        -121.977577,37.340172,0.000000
        -121.977409,37.339642,0.000000
        -121.976753,37.338360,0.000000
        -121.976753,37.338360,0.000000
        -121.978180,37.337952,0.000000
        -121.979050,37.337898,0.000000
        -121.986267,37.337860,0.000000
        -121.986267,37.337860,0.000000
        -121.986313,37.341080,0.000000
        -121.986259,37.341450,0.000000
        -121.986259,37.341450,0.000000
        -121.987663,37.341599,0.000000
        -121.987663,37.341599,0.000000
        -121.986771,37.347130,0.000000
        -121.986778,37.350479,0.000000
        -121.986778,37.350479,0.000000
        -121.987633,37.350422,0.000000
        -121.989906,37.349819,0.000000
        -121.991760,37.349152,0.000000
        -121.994011,37.349098,0.000000
'''

dmv_test_1 = '''-121.994202,37.349098,0.000000
        -121.991760,37.349152,0.000000
        -121.989906,37.349819,0.000000
        -121.987633,37.350422,0.000000
        -121.986778,37.350479,0.000000
        -121.986778,37.350479,0.000000
        -121.986748,37.349121,0.000000
        -121.986748,37.349121,0.000000
        -121.977638,37.349140,0.000000
        -121.977638,37.349140,0.000000
        -121.977669,37.346958,0.000000
        -121.977577,37.340172,0.000000
        -121.977409,37.339642,0.000000
        -121.976753,37.338360,0.000000
        -121.976753,37.338360,0.000000
        -121.978180,37.337952,0.000000
        -121.979050,37.337898,0.000000
        -121.986267,37.337860,0.000000
        -121.986267,37.337860,0.000000
        -121.986313,37.341080,0.000000
        -121.986259,37.341450,0.000000
        -121.986259,37.341450,0.000000
        -121.987663,37.341599,0.000000
        -121.987663,37.341599,0.000000
        -121.987091,37.345169,0.000000
        -121.987091,37.345169,0.000000
        -121.993340,37.345032,0.000000
        -121.993767,37.345100,0.000000
        -121.994278,37.345329,0.000000
        -121.994278,37.345329,0.000000
        -121.993950,37.345638,0.000000
        -121.993393,37.345879,0.000000
        -121.993156,37.346218,0.000000
        -121.993134,37.346500,0.000000
        -121.993233,37.349110,0.000000
        -121.993233,37.349110,0.000000
        -121.994202,37.349098,0.000000
        '''
dmv_test_3='''-121.994072,37.349098,0.000000
        -121.991760,37.349152,0.000000
        -121.989906,37.349819,0.000000
        -121.987633,37.350422,0.000000
        -121.986778,37.350479,0.000000
        -121.986778,37.350479,0.000000
        -121.986748,37.349121,0.000000
        -121.986748,37.349121,0.000000
        -121.984657,37.349129,0.000000
        -121.984657,37.349129,0.000000
        -121.984650,37.350422,0.000000
        -121.984756,37.350681,0.000000
        -121.984756,37.350681,0.000000
        -121.984177,37.351002,0.000000
        -121.983917,37.351261,0.000000
        -121.983788,37.351639,0.000000
        -121.983788,37.352440,0.000000
        -121.983788,37.352440,0.000000
        -121.986771,37.352470,0.000000
        -121.994942,37.352421,0.000000
        -121.994942,37.352421,0.000000
        -121.995621,37.352612,0.000000
        -121.995689,37.352669,0.000000
        -121.995750,37.352879,0.000000
        -121.995750,37.352879,0.000000
        -121.995796,37.354191,0.000000
        -121.995796,37.354191,0.000000
        -121.986732,37.354279,0.000000
        -121.986732,37.354279,0.000000
        -121.986801,37.353889,0.000000
        -121.986771,37.352470,0.000000
        -121.986771,37.352470,0.000000
        -121.992821,37.352428,0.000000
        -121.992821,37.352428,0.000000
        -121.992859,37.352280,0.000000
        -121.993332,37.351688,0.000000
        -121.993423,37.351452,0.000000
        -121.993370,37.350231,0.000000
        -121.993240,37.349709,0.000000
        -121.993233,37.349110,0.000000
        -121.993233,37.349110,0.000000
        -121.994072,37.349098,0.000000
'''
#run(around_company)
#run(dmv_test_2)
#run(dmv_test_1)
run(dmv_test_3)